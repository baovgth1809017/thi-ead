package com.example.demo.service;


import com.example.demo.entity.Candidate;
import com.example.demo.repository.CandidateRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class CandidateService {
    private final CandidateRepository candidateRepository;

    public CandidateService(CandidateRepository candidateRepository) {
        this.candidateRepository = candidateRepository;
    }

    public List<Candidate> findAll(){
        return  candidateRepository.findAll();
    }
    public Optional<Candidate> findById(Long id){
        return candidateRepository.findById(id);
    }
    public Candidate save(Candidate cand){
        return candidateRepository.save(cand);
    }
    public void deleteById(Long id){
        candidateRepository.deleteById(id);
    }
}
