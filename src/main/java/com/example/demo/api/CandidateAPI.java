package com.example.demo.api;


import com.example.demo.entity.Candidate;
import com.example.demo.service.CandidateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/candidates")
@RequiredArgsConstructor
public class CandidateAPI {
    private final CandidateService candidateService;


    @GetMapping
    public ResponseEntity<List<Candidate>> findAll(){
        return ResponseEntity.ok(candidateService.findAll());
    }
    @PostMapping ResponseEntity create(@Valid @RequestBody Candidate candidate){
        return ResponseEntity.ok(candidateService.save(candidate));
    }
    @GetMapping("/{id}")
    public ResponseEntity<Candidate> findById(@PathVariable Long id){
        Optional<Candidate> cand = candidateService.findById(id);
        if (!cand.isPresent()){
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(cand.get());
    }
    @PutMapping("/{id}")
    public ResponseEntity<Candidate> update(@PathVariable Long id,
                                            @Valid @RequestBody Candidate candidate){
        Optional<Candidate> cand = candidateService.findById(id);
        if (!cand.isPresent()){
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(cand.get());
    }
    @DeleteMapping("/{id}")
    public  ResponseEntity<Candidate> delete(@PathVariable Long id,
                                             @Valid @RequestBody Candidate candidate){
        Optional<Candidate> cand = candidateService.findById(id);
        if (!cand.isPresent()){
            ResponseEntity.badRequest().build();
        }
        candidateService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
